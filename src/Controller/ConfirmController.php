<?php

namespace Drupal\netlify_node_endpoints\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Session\AccountInterface;
use Drupal\node\Entity\Node;
use Drupal\node\NodeTypeInterface;

class ConfirmController extends ControllerBase {

  private $lastDeployment;

  public function __construct() {
    $this->lastDeployment = NULL;
  }

  public function access(AccountInterface $account, NodeTypeInterface $node_type, $env) {
    $user_roles = $account->getRoles();
    $allowed_roles = $node_type->getThirdPartySetting('netlify_node_endpoints', "netlify_{$env}_roles");
    if (!empty(array_intersect($allowed_roles, $user_roles))) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }

  public function content(NodeTypeInterface $node_type, $env) {
    $build = [];

    // Last of nodes updated since last Netlify webhook.
    $nids = $this->getChangedNodes($node_type, $env);
    $nodes_markup = '<div>';
    $nodes_markup .= '<h2>Updates since last manual deployment via Drupal</h2>';
    if (!empty($nids)) {
      $nodes_markup .= '<p>Please note that this does not consider deleted nodes and deployments recently triggered by Netlify.</p>';
      $nodes_markup .= '<ul>';

      $nodes = Node::loadMultiple($nids);
      foreach ($nodes as $node) {
        $nodes_markup .= '<li>' . $node->toLink()->toString() . ' - Updated: ' . \Drupal::service('date.formatter')->formatTimeDiffSince($node->getChangedTime()) . ' ago by ' . $node->getRevisionAuthor()->getDisplayName() . '</li>';
      }
      $nodes_markup .= '</ul>';
    }
    else {
      $nodes_markup .= 'No node updates since last deployment to ' . $env . ' ' . \Drupal::service('date.formatter')->formatTimeDiffSince($this->getLastDeployment($env)) . ' ago.';
    }
    $nodes_markup .= '</div><br>';
    $build['nodes'] = [
      '#type' => 'markup',
      '#markup' => $nodes_markup,
    ];

    // Confirmation message.
    $build['message'] = [
      '#type' => 'markup',
      '#markup' => Markup::create($this->t('Are you sure you want to trigger Netlify deploy of @ct to @env?', [
        '@ct' => $node_type->label(),
        '@env' => $env,
      ])),
    ];

    // Confirmation button.
    $build['form'] = \Drupal::formBuilder()->getForm('Drupal\netlify_node_endpoints\Form\DeployForm', $node_type->id(), $env);

    return $build;
  }

  public function title(NodeTypeInterface $node_type, $env) {
    return $this->t('Netlify deploy confirmation to <em>@env</em>', ['@env' => $env]);
  }

  private function getLastDeployment($env) {
    if ($this->lastDeployment === NULL) {
      $this->lastDeployment = \Drupal::state()->get("netlify_node_endpoints_last_updated:{$env}", -1);
    }
    return $this->lastDeployment;
  }

  private function getChangedNodes(NodeTypeInterface $node_type, $env) {
    return \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', $node_type->id())
      ->condition('changed', $this->getLastDeployment($env), '>')
      ->sort('changed', 'DESC')
      ->execute();
  }

}
