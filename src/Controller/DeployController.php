<?php

namespace Drupal\netlify_node_endpoints\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;

class DeployController extends ControllerBase {

  /**
   * Display the page /admin/netlify-node-endpoints/deploy.
   *
   * @return mixed
   */
  public function content() {
    $user_roles = \Drupal::currentUser()->getRoles();
    $node_types = NodeType::loadMultiple();
    $node_keys = array_keys($node_types);
    $envs = ['staging', 'production'];
    $has_access_to_any_node_type = FALSE;
    $build = [];

    // Go through content type and build rows.
    for ($i = 1; $i <= count($node_keys); $i++) {
      $node_type = $node_types[$node_keys[($i - 1)]];
      $all_roles = [];

      foreach ($envs as $env) {
        $endpoints[$env] = $node_type->getThirdPartySetting('netlify_node_endpoints', "netlify_{$env}_endpoint");
        $roles[$env] = $node_type->getThirdPartySetting('netlify_node_endpoints', "netlify_{$env}_roles");
        $all_roles += $roles[$env];
      }

      // If user has permissions and there are endpoints.
      if (array_intersect($user_roles, $all_roles) && !empty($endpoints)) {
        // Flag the user as having access and build table headers (only once).
        if ($has_access_to_any_node_type === FALSE) {
          $has_access_to_any_node_type = TRUE;

          // Table headers.
          $build['table'] = [
            '#type' => 'table',
            '#header' => [
              $this->t('Content type'),
              $this->t('Operations'),
            ],
          ];
        }

        $build['table'][$i]['content_type'] = [
          '#type' => 'markup',
          '#markup' => Markup::create($this->t($node_type->label())),
        ];

        $build['table'][$i]['extra_actions']['#type'] = 'dropbutton';

        foreach ($envs as $env) {
          if (!empty($endpoints[$env]) && array_intersect($user_roles, $roles[$env])) {
            $build['table'][$i]['extra_actions']['#links'][$env] = [
              'title' => $this->t('Deploy to @env', ['@env' => $env]),
              'url' => Url::fromRoute('netlify_node_endpoints.confirm', ['node_type' => $node_type->id(), 'env' => $env]),
            ];
          }
        }
      }
    }

    // If they didn't have access to any node types...
    if ($has_access_to_any_node_type === FALSE) {
      $build['empty_message'] = [
        '#type' => 'markup',
        '#markup' => Markup::create($this->t('There are no Netlify endpoints configured for your role. These are configured on each content type edit page.')),
      ];
    }

    return $build;
  }

}
