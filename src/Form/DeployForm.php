<?php

namespace Drupal\netlify_node_endpoints\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\netlify_node_endpoints\SendToNetlify;
use Drupal\node\Entity\NodeType;

class DeployForm extends FormBase {

  public function getFormId() {
    return 'netlify_node_endpoints_deploy_form';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $node_type = NULL, $env = NULL) {
    $form['node_type'] = [
      '#type' => 'hidden',
      '#value' => $node_type,
    ];
    $form['env'] = [
      '#type' => 'hidden',
      '#value' => $env,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Deploy to @env', ['@env' => $env]),
      '#button_type' => 'primary',
    ];
    $form['actions']['goback'] = [
      '#type' => 'submit',
      '#value' => $this->t('Go back'),
      '#button_type' => 'secondary',
    ];
    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check permissions in here.
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    $input = $form_state->getUserInput();
    $redirect_to_route = 'netlify_node_endpoints.deploy';

    if ($input['op'] === 'Go back') {
      $form_state->setRedirect($redirect_to_route);
      return TRUE;
    }

    $node_type = NodeType::load($input['node_type']);
    if ($node_type === NULL) {
      $this->messenger()->addError($this->t('That content type does not exist. Cannot deploy.'));
      $form_state->setRedirect($redirect_to_route);
      return FALSE;
    }

    // Send webhook.
    if ((new SendToNetlify($node_type))->execute($input['env']) === TRUE) {
      $this->messenger()->addStatus('Deploy hook successfully sent to Netlify.');
      $form_state->setRedirect($redirect_to_route);
      return TRUE;
    }
    else {
      $this->messenger()->addError('cURL request was unsuccessful.');
      $form_state->setRedirect($redirect_to_route);
      return FALSE;
    }
  }

}
