<?php

namespace Drupal\netlify_node_endpoints;

use Drupal\node\NodeTypeInterface;
use GuzzleHttp\Exception\RequestException;

class SendToNetlify {

  private $nodeType;
  private $endpoints;
  private $lastDeployment;

  public function __construct(NodeTypeInterface $node_type) {
    $this->nodeType = $node_type;
    foreach (['staging', 'production'] as $env) {
      $this->endpoints[$env] = $node_type->getThirdPartySetting('netlify_node_endpoints', "netlify_{$env}_endpoint");
      $this->lastDeployment[$env] = \Drupal::state()->get("netlify_node_endpoints_last_updated:{$env}", -1);
    }
  }

  public function hasEndpoint($env) {
    return !empty($this->endpoints[$env]);
  }

  public function execute($env) {
    if ($this->curl($this->endpoints[$env]) === TRUE) {
      \Drupal::state()->set("netlify_node_endpoints_last_updated:{$env}", \Drupal::time()->getCurrentTime());
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Wrapper for a cURL request.
   *
   * @param string $endpoint
   *   URL endpoint to hit.
   *
   * @return bool
   */
  private function curl($endpoint) {
    $client = \Drupal::httpClient();

    // Try the POST request.
    try {
      $request = $client->post($endpoint);
    }
    catch (RequestException $e) {
      $this->messenger()->addError($e->getMessage());
      return FALSE;
    }

    // If we didn't get a 200 at this point, something went wrong.
    if ($request->getStatusCode() !== 200) {
      $this->messenger()->addError('cURL returned with status code: @code', ['@code' => $request->getStatusCode()]);
      return FALSE;
    }

    return TRUE;
  }

}
